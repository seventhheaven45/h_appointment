FROM python:3.9
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD web/requirements.txt /code/
RUN pip install -r requirements.txt
ADD web/ /code/
