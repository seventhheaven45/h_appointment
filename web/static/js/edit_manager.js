$(function() {
  console.log($('input[name="is_authorized"]').prop("checked"));
  console.log($('input[name="is_system_manager"]').prop("checked"));
  $('input[name="is_authorized"]').change(function(){
    if($(this).prop('checked')){
      $('#btn_label_is_authorized').html('<i class="fas fa-user-check"></i>');
    }else{
      $('#btn_label_is_authorized').html('<i class="fas fa-user-slash"></i>');
    }
  });

  $('input[name="is_system_manager"]').change(function(){
    if($(this).prop('checked')){
      $('#btn_label_is_system_manager').html('<i class="fas fa-user-check"></i>');
    }else{
      $('#btn_label_is_system_manager').html('<i class="fas fa-user-slash"></i>');
    }
  });

//  $('button#submit_edit_manager').on('click', function(){
//    $('input').prop('disabled', false);
//    $('#edit_manager_form').submit();
//  });
});
