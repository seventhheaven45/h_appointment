import base64
from functools import wraps

from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth import authenticate
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.shortcuts import redirect
from django.views.decorators.http import require_http_methods

from appointment.models import Customer
from management.models import Manager


def customer_login_required(func):
    @wraps(func)
    def wrapper(request, *args, **kwargs):
        customer_id = request.session.get('customer_id')
        if not customer_id:
            return redirect('appointment:login')
        try:
            customer = Customer.objects.get(id=customer_id)
        except (MultipleObjectsReturned, ObjectDoesNotExist):
            del request.session['customer_id']
            return redirect('appointment:login')
        request.customer = customer
        return func(request, *args, **kwargs)
    return wrapper


def manager_login_required(func):
    @wraps(func)
    def wrapper(request, *args, **kwargs):
        manager_id = request.session.get('manager_id')
        if not manager_id:
            return redirect('management:login')
        try:
            manager = Manager.objects.get(id=manager_id)
        except (MultipleObjectsReturned, ObjectDoesNotExist):
            messages.error(request, "不正な管理者IDです。")
            del request.session['manager_id']
            return redirect('management:login')
        if not manager.is_authorized:
            messages.error(request, "管理者承認がされていません。")
            del request.session['manager_id']
            return redirect('management:login')
        request.manager = manager
        return func(request, *args, **kwargs)
    return wrapper


def _basic_auth(request):
    if 'HTTP_AUTHORIZATION' not in request.META:
        return False
    (auth_scheme, base64_username_pass) = request.META['HTTP_AUTHORIZATION'].split(' ', 1)
    if auth_scheme.lower() != 'basic':
        return False
    username_pass = base64.decodebytes(base64_username_pass.strip().encode('ascii')).decode('ascii')
    (username, password) = username_pass.split(':', 1)
    user = authenticate(username=username, password=password)
    return user is not None


def basic_auth(func):
    @wraps(func)
    def wrapper(request, *args, **kwargs):
        if not request.manager.is_system_manager:
            messages.warning(request, "システムマネージャーのみ検閲可能なページです。")
            return redirect('management:login')
        if not _basic_auth(request):
            response = HttpResponse("Unauthorized", status=401)
            response['WWW-Authenticate'] = 'Basic realm="basic auth username/password invalid"'
            return response
        else:
            return func(request, *args, **kwargs)
    return wrapper


require_GET_POST = require_http_methods(["GET", "POST"])
require_GET_POST.__doc__ = "Decorator to require that a view only accepts the GET and POST method."
