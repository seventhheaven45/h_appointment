from django.db import models


class UndeletedManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(deleted_at__isnull=True)


class BaseModel(models.Model):
    """ベースモデル"""
    created_at = models.DateTimeField('作成日時', auto_now_add=True)
    updated_at = models.DateTimeField('更新日時', auto_now=True)
    deleted_at = models.DateTimeField('論理削除日', blank=True, null=True)

    objects = UndeletedManager()
    all_objects = models.Manager()

    class Meta:
        abstract = True
