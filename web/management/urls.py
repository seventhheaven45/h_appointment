from django.urls import path
from . import views

app_name = 'appointment'

urlpatterns = [
    path('', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('create_temporary_manager/', views.create_temporary_manager, name='create_temporary_manager'),
    path('create_manager/<str:token>/', views.create_manager, name='create_manager'),
    path('done_create_manager/', views.done_create_manager, name='done_create_manager'),
    path('appointment_list/', views.appointment_list, name='appointment_list'),
    path('manager_list/', views.manager_list, name='manager_list'),
    path('edit_manager/<int:manager_id>', views.edit_manager, name='edit_manager'),
    path('delete_manager/<int:manager_id>', views.delete_manager, name='delete_manager'),
]