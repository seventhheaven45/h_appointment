from django.contrib import admin
from management.models import Manager, TemporaryManager

admin.site.register(Manager)
admin.site.register(TemporaryManager)
