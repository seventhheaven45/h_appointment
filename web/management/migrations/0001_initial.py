# Generated by Django 3.2 on 2021-08-28 09:22

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TemporaryManager',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='作成日時')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='更新日時')),
                ('deleted_at', models.DateTimeField(blank=True, null=True, verbose_name='論理削除日')),
                ('name', models.CharField(max_length=30, verbose_name='名前')),
                ('mail_address', models.EmailField(max_length=255, verbose_name='メールアドレス')),
                ('token', models.CharField(max_length=255, verbose_name='トークン')),
                ('token_expired_at', models.DateTimeField(verbose_name='トークン有効期限')),
            ],
            options={
                'verbose_name_plural': '本登録前管理者',
                'db_table': 'temporary_manager',
            },
        ),
        migrations.CreateModel(
            name='Manager',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='作成日時')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='更新日時')),
                ('deleted_at', models.DateTimeField(blank=True, null=True, verbose_name='論理削除日')),
                ('name', models.CharField(max_length=30, verbose_name='名前')),
                ('password', models.CharField(max_length=255, verbose_name='パスワード')),
                ('mail_address', models.EmailField(max_length=255, unique=True, verbose_name='メールアドレス')),
                ('is_authorized', models.BooleanField(default=False, verbose_name='承認済みフラグ')),
                ('is_system_manager', models.BooleanField(default=False, verbose_name='システム管理者フラグ')),
            ],
            options={
                'verbose_name_plural': '管理者',
                'db_table': 'manager',
                'unique_together': {('name', 'password')},
            },
        ),
    ]
