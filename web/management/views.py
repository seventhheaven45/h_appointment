import hashlib
from datetime import datetime

from dateutil.relativedelta import relativedelta

from django.contrib import messages
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.http.response import HttpResponse
from django.http.request import HttpRequest
from django.shortcuts import redirect, render
from django.views.decorators.http import require_GET, require_POST

from appointment.models import Appointment
from core.decorators import basic_auth, require_GET_POST, manager_login_required
from management.forms import (
    AppointmentSearchForm,
    EditManagerForm,
    LoginForm,
    TemporaryManagerForm,
    ManagerForm,
    ManagerSearchForm,
)
from management.models import Manager, TemporaryManager
from management.utils import make_page_obj, make_sha512, send_mail_temp_user_registration_done


@require_GET_POST
def login(request: HttpRequest):
    """ログイン"""
    if request.session.get('manager_id'):
        return redirect('management:appointment_list')
    form = LoginForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            manager = Manager.objects.get(name=form.cleaned_data['name'])
            request.session["manager_id"] = manager.id
            return redirect('management:appointment_list')

    return render(request, 'management/no_menu/login.html', {'form': form})


@manager_login_required
@require_POST
def logout(request: HttpRequest):
    """ログアウト"""
    if request.session.get('manager_id'):
        del request.session["manager_id"]
    return redirect('management:login')


@require_GET_POST
def create_temporary_manager(request: HttpRequest):
    """管理者仮登録"""
    form = TemporaryManagerForm(request.POST or None)
    if request.method == "POST":
        form.is_valid()
        if form.is_valid():
            cleaned_data = form.cleaned_data
            name = cleaned_data['name']
            mail_address = cleaned_data['mail_address']
            # トークン作成
            now = datetime.now()
            token_str = name + mail_address + str(now)
            token = make_sha512(token_str)
            token_expired_at = now + relativedelta(minutes=30)
            # 仮登録ユーザー作成
            # メールアドレスが有効か確認
            is_success = send_mail_temp_user_registration_done(request, token, token_expired_at, mail_address)
            if is_success:
                # DBに仮管理者ユーザーを登録
                TemporaryManager.objects.create(
                    name=name,
                    mail_address=mail_address,
                    token=token,
                    token_expired_at=token_expired_at,
                )
                messages.success(request, '本登録案内メールを送信しました。')
                messages.success(request, 'メールフォルダをご確認ください。')
                return redirect('management:create_temporary_manager')
            else:
                messages.error(request, 'メール送信に失敗しました。\nメールアドレスが正しいかご確認ください。')
                return redirect('management:create_temporary_manager')

    return render(request, 'management/no_menu/create_temporary_manager.html', {'form': form})


@require_GET_POST
def create_manager(request: HttpRequest, token: str):
    """管理者本登録"""
    now = datetime.now()
    try:
        temp_manager = TemporaryManager.objects.get(token=token, token_expired_at__gte=now)
    except (MultipleObjectsReturned, ObjectDoesNotExist):
        return HttpResponse("無効なトークンです。")

    form = ManagerForm(temp_manager.name, request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            cleaned_data = form.cleaned_data
            password = cleaned_data.get('password')
            Manager.objects.create(
                name=temp_manager.name,
                password=hashlib.sha512(password.encode('utf-8')).hexdigest(),
                mail_address=temp_manager.mail_address,
                )
            temp_manager.delete()
            return redirect('management:done_create_manager')

    return render(request, 'management/no_menu/create_manager.html', {'form': form})


@require_GET
def done_create_manager(request: HttpRequest):
    return render(request, 'management/no_menu/done_create_manager.html')


@require_GET_POST
@manager_login_required
def appointment_list(request: HttpRequest):
    now = datetime.now()
    appointment_qs = None
    form = AppointmentSearchForm(request.GET or None)
    if form:
        if form.is_valid():
            cleaned_data = form.cleaned_data
            only_my_customer_flg = cleaned_data["only_my_customer_flg"]
            from_visit_date = cleaned_data["from_visit_date"]
            to_visit_date = cleaned_data["to_visit_date"]

            appointment_qs = Appointment.objects.all()
            if only_my_customer_flg:
                pass
                # appointment_qs.filter(manager=request.manager)
            if from_visit_date:
                appointment_qs.filter(visit_at__gte=from_visit_date)
            if to_visit_date:
                appointment_qs.filter(manager=to_visit_date)
    if not appointment_qs:
        appointment_qs = Appointment.objects.filter(visit_at__range=(now, now+relativedelta(months=1)))

    page_range = 20
    page_obj = make_page_obj(appointment_qs, page_range, request.GET.get('page'))

    ret = {"form": form, "page_obj": page_obj}

    return render(request, 'management/menu/appointment_list.html', ret)


@require_GET
@manager_login_required
@basic_auth
def manager_list(request: HttpRequest):
    manager_qs = Manager.objects.all().order_by('name')
    form = ManagerSearchForm(request.GET or None)
    if form.is_valid():
        only_not_authorized_flg = form.cleaned_data["only_not_authorized_flg"]
        if only_not_authorized_flg:
            manager_qs = manager_qs.filter(is_authorized=False)
    print(manager_qs)
    page_range = 20
    page_obj = make_page_obj(manager_qs, page_range, request.GET.get('page'))
    print(page_obj)

    ret = {"form": form, "page_obj": page_obj}

    return render(request, 'management/menu/manager_list.html', ret)


@require_GET_POST
@manager_login_required
@basic_auth
def edit_manager(request: HttpRequest, manager_id: int):
    try:
        manager = Manager.objects.get(id=manager_id)
    except (ObjectDoesNotExist, MultipleObjectsReturned):
        messages.error(request, "該当するマネージャーが存在しません。")
        return redirect('management:manager_list')
    initial_dict = {
        "name": manager.name,
        "mail_address": manager.mail_address,
        "is_authorized": manager.is_authorized,
        "is_system_manager": manager.is_system_manager,
    }
    form = EditManagerForm(
        request.manager.id,
        manager_id,
        initial_dict,
        request.POST or None,
    )
    if request.method == "POST":
        if form.is_valid():
            cleaned_data = form.cleaned_data
            manager.name = cleaned_data["name"]
            manager.mail_address = cleaned_data["mail_address"]
            manager.is_authorized = cleaned_data["is_authorized"]
            if request.manager == manager:
                manager.is_system_manager = cleaned_data["hidden_is_system_manager"]
            else:
                manager.is_system_manager = cleaned_data["is_system_manager"]
            manager.save()
            messages.success(request, "プロフィールが更新されました。")
            return redirect('management:edit_manager', manager.id)
    return render(request, 'management/menu/edit_manager.html', {'form': form, 'manager': manager})


@require_POST
@manager_login_required
def delete_manager(request: HttpRequest, manager_id: int):
    manager = Manager.objects.get(id=manager_id)
    delete_count = manager.delete()
    if delete_count > 0:
        messages.success(request, "id: {} {}さんのユーザー削除に成功しました。".format(manager.id, manager.name))
    else:
        messages.error(request, "id: {} {}さんのユーザー削除が失敗しました。".format(manager.id, manager.name))
    return redirect('management:manager_list')
