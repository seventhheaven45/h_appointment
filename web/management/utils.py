import hashlib
from datetime import datetime

from django.conf import settings
from django.core.mail import send_mail
from django.core.paginator import Paginator
from django.shortcuts import redirect
from django.http.request import HttpRequest


def make_sha512(data: str):
    return hashlib.sha512(data.encode('utf-8')).hexdigest()


def send_mail_temp_user_registration_done(request: HttpRequest, token: hash,
                                          token_expired_at: datetime, to_mail_address: str):
    return send_mail(
        # subject
        "来店予約システム / ユーザー仮登録完了のお知らせ。",
        # message
        "管理者ユーザーの仮登録が完了しました。\n"
        "以下のURLより本登録を行ってください。\n"
        "※有効期限: {}\n"
        "{}://{}{}".format(
            token_expired_at.strftime('%Y/%m/%d %H:%M:%S'),
            request.scheme,
            request.get_host(),
            redirect("management:create_manager", token=token).url,
        ),
        # from_email
        settings.EMAIL_HOST_USER,
        # to_email
        [to_mail_address],
    )


def make_page_obj(objects, page_range: int, page: int or None):
    paginator = Paginator(objects, page_range)
    page_number = page
    page_obj = paginator.get_page(page_number)
    return page_obj

