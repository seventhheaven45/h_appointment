from datetime import datetime

from django import forms
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist, ValidationError

from management.models import Manager, TemporaryManager
from management.utils import make_sha512


class LoginForm(forms.Form):
    name = forms.CharField(label="名前", max_length="30")
    password = forms.CharField(label="パスワード", max_length="255", widget=forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control mt-1'

    def clean(self):
        cleaned_data = self.cleaned_data
        name = cleaned_data.get('name')
        password = cleaned_data.get('password')
        try:
            manager = Manager.objects.get(name=name, password=make_sha512(password))
            if not manager.is_authorized:
                self.add_error(None, "まだ承認されていません。")
        except ObjectDoesNotExist:
            self.add_error('password', "ユーザーネームまたはパスワードが間違っています。")
        except MultipleObjectsReturned:
            self.add_error('password', "同一アカウントが複数存在します。\n管理者にご連絡ください。")

        return cleaned_data


class TemporaryManagerForm(forms.Form):
    name = forms.CharField(label="名前", max_length="30")
    mail_address = forms.EmailField(label="メールアドレス", max_length="255")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control mt-1'

    def clean_mail_address(self):
        now = datetime.now()
        mail_address = self.cleaned_data['mail_address']
        if Manager.objects.filter(mail_address=mail_address).exists():
            self.add_error('mail_address', "このメールアドレスは登録されています。")
        elif TemporaryManager.objects.filter(mail_address=mail_address, token_expired_at__gte=now).exists():
            self.add_error('mail_address', "送信済みの本登録案内メールが有効期限内です。\nメールボックスをご確認ください。")

        return mail_address


class ManagerForm(forms.Form):
    password = forms.CharField(label="パスワード", max_length="255", widget=forms.PasswordInput())
    conf_password = forms.CharField(label="パスワード（確認）", max_length="255", widget=forms.PasswordInput())

    def __init__(self, temp_manager_name, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.temp_manager_name = temp_manager_name
        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control mt-1'

    def clean(self):
        cleaned_data = self.cleaned_data
        password = cleaned_data.get('password')
        conf_password = cleaned_data.get('conf_password')
        if not password == conf_password:
            self.add_error('conf_password', "パスワードが一致しません。")
        elif Manager.objects.filter(name=self.temp_manager_name, password=make_sha512(password)).exists():
            self.add_error('conf_password', "このパスワードはご使用できません。")

        return cleaned_data


class AppointmentSearchForm(forms.Form):
    only_my_customer_flg = forms.BooleanField(
        label="自分の担当顧客だけを表示",
        label_suffix='',
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'class': 'form-check-input me-2',
                'type': 'checkbox',
            }),
    )
    from_visit_date = forms.DateField(
        label="いつから",
        required=False,
        input_formats=['%Y/%m/%d'],
        widget=forms.TextInput(
            attrs={
                'class': 'form-control datetimepicker-input from-date-picker',
                'data-target': '#from-date-picker',
                'placeholder': 'YYYY/mm/dd',
                'autocomplete': 'off',
            }
        )
    )
    to_visit_date = forms.DateField(
        label="いつまで",
        required=False,
        input_formats=['%Y/%m/%d'],
        widget=forms.TextInput(
            attrs={
                'class': 'form-control datetimepicker-input to-date-picker',
                'data-target': '#to-date-picker',
                'placeholder': 'YYYY/mm/dd',
                'autocomplete': 'off',
            }
        )
    )

    def clean(self):
        cleaned_data = self.cleaned_data
        print("cleaned_data", cleaned_data)
        from_visit_date = cleaned_data["from_visit_date"]
        to_visit_date = cleaned_data["to_visit_date"]
        if from_visit_date and to_visit_date and to_visit_date < from_visit_date:
            self.add_error('to_visit_date', "「いつまで」の日付は「いつから」より後日にしてください。")

        return cleaned_data


class ManagerSearchForm(forms.Form):
    only_not_authorized_flg = forms.BooleanField(
        label='未承認の管理者のみ表示',
        label_suffix='',
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'class': 'form-check-input me-2',
                'type': 'checkbox',
            }),
    )


class EditManagerForm(forms.Form):
    name = forms.CharField(
        label='名前',
        max_length="30",
        widget=forms.TextInput(
            attrs={
                'class': 'form-control mt-1',
            }
        )
    )
    mail_address = forms.EmailField(
        label="メールアドレス",
        max_length="255",
        widget=forms.TextInput(
            attrs={
                'class': 'form-control mt-1',
            }
        )
    )
    is_authorized = forms.BooleanField(
        label="管理者承認",
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'class': 'btn-check',
                'type': 'checkbox',
            }),
    )
    is_system_manager = forms.BooleanField(
        label="システムマネージャー",
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'class': 'btn-check',
                'type': 'checkbox',
            }),
    )
    hidden_is_system_manager = forms.BooleanField(
        required=False,
        widget=forms.HiddenInput,
    )

    def __init__(self, login_manager_id: int, manager_id: int, initial_dict: dict, *args, **kwargs):
        super().__init__(*args, **kwargs)
        manager = Manager.objects.get(id=manager_id)
        self.before_is_system_manager = manager.is_system_manager
        self.login_manager_id = login_manager_id
        self.manager_id = manager_id
        for k, v in initial_dict.items():
            self.fields[k].initial = v
        if login_manager_id == manager_id:
            self.fields['is_system_manager'].widget.attrs['disabled'] = 'disabled'
            self.fields['hidden_is_system_manager'].initial = self.fields['is_system_manager'].initial
        else:
            del self.fields['hidden_is_system_manager']
