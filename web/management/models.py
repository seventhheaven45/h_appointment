from django.db import models
from django.utils import timezone

from core.models import BaseModel


class TemporaryManager(BaseModel):
    name = models.CharField('名前', max_length=30)
    mail_address = models.EmailField('メールアドレス', max_length=255)
    token = models.CharField('トークン', max_length=128)
    token_expired_at = models.DateTimeField('トークン有効期限')

    class Meta:
        db_table = "temporary_manager"
        verbose_name_plural = "本登録前管理者"

    def __str__(self):
        return '{}（有効期限：{}）'.format(
            self.name, timezone.localtime(self.token_expired_at).strftime("%Y/%m/%d %H:%M:%S"))


class Manager(BaseModel):
    name = models.CharField('名前', max_length=30)
    password = models.CharField('パスワード', max_length=255)
    mail_address = models.EmailField('メールアドレス', max_length=255, unique=True)
    is_authorized = models.BooleanField('承認済みフラグ', default=False)
    is_system_manager = models.BooleanField('システム管理者フラグ', default=False)

    class Meta:
        db_table = "manager"
        verbose_name_plural = "管理者"
        unique_together = ["name", "password"]

    def __str__(self):
        return self.name
