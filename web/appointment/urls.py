from django.urls import path
from . import views

app_name = 'appointment'

urlpatterns = [
    path('', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('make_appointment/', views.make_appointment, name='make_appointment'),
    path('delete_appointment/', views.delete_appointment, name='delete_appointment'),
]