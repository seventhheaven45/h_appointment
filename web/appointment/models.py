from django.db import models
from django.core.validators import MinLengthValidator

from core.models import BaseModel


class Customer(BaseModel):
    """顧客"""
    name = models.CharField('名前', max_length=50)
    phone_number = models.CharField('電話番号', validators=[MinLengthValidator(10)], max_length=12)

    class Meta:
        db_table = "customer"
        verbose_name_plural = "顧客"
        unique_together = ["name", "phone_number"]

    def __str__(self):
        return self.name


class Car(BaseModel):
    """車"""
    name = models.CharField('名前', max_length=255)
    number = models.CharField('ナンバープレート4桁', validators=[MinLengthValidator(4)], max_length=4)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)

    class Meta:
        db_table = "car"
        verbose_name_plural = "車"

    def __str__(self):
        return "{}/{}（{}）".format(self.name, self.number, self.customer.name)


class Appointment(BaseModel):
    """来店予約"""
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    visit_at = models.DateTimeField("予約日時")
    visiting_purpose = models.CharField('来店理由', max_length=255)

    class Meta:
        db_table = "appointment"
        verbose_name_plural = "来店予約"

    def __str__(self):
        return "{}（{}）".format(self.customer.name, self.visit_at)
