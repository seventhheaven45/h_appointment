from django.contrib import admin
from appointment.models import Appointment, Car, Customer

admin.site.register(Appointment)
admin.site.register(Car)
admin.site.register(Customer)
