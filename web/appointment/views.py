from datetime import datetime

from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned, ValidationError
from django.http import HttpResponse
from django.views.decorators.http import require_POST
from django.shortcuts import redirect, render

from appointment.forms import AppointmentForm, LoginForm
from appointment.models import Customer, Appointment
from core.decorators import customer_login_required, require_GET_POST


@require_GET_POST
def login(request):
    """ログイン"""
    if request.session.get('customer_id'):
        return redirect('appointment:make_appointment')
    form = LoginForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            name = form.cleaned_data['name']
            phone_number = form.cleaned_data['phone_number']
            customer = Customer.objects.get(name=name, phone_number=phone_number)
            request.session["customer_id"] = customer.id
            return redirect('appointment:make_appointment')

    return render(request, 'appointment/login.html', {'form': form})


@customer_login_required
@require_POST
def logout(request):
    """ログアウト"""
    if request.session.get('customer_id'):
        del request.session["customer_id"]
    return redirect('appointment:login')


@customer_login_required
@require_GET_POST
def make_appointment(request):
    """来店予約"""
    now = datetime.now()
    form = AppointmentForm(request.customer.id, request.POST or None)

    if request.method == "POST":
        if form.is_valid():
            visit_date = form.cleaned_data['visit_date']
            visit_time = form.cleaned_data['visit_time']
            visiting_purpose = form.cleaned_data['visiting_purpose']
            visit_at = datetime.combine(visit_date, visit_time)
            Appointment.objects.create(
                customer=request.customer,
                visit_at=visit_at,
                visiting_purpose=visiting_purpose,
            )
            messages.success(request, "ご来店を予約しました。")
            return redirect('appointment:make_appointment')

    appointment = None
    try:
        appointment = Appointment.objects.get(
            customer=request.customer,
            visit_at__gte=now,
        )
        for field in form.fields.values():
            field.widget.attrs['disabled'] = 'disabled'
    except MultipleObjectsReturned:
        Appointment.objects.filter(
            customer=request.customer,
            visit_at__gte=now,
        ).delete()
        return HttpResponse('複数のご予約が見つかりました。<br>画面をリロードし、再度ご予約ください。')
    except ObjectDoesNotExist:
        pass

    ret = {'form': form, 'appointment': appointment}

    return render(request, 'appointment/appointment.html', ret)


@customer_login_required
@require_POST
def delete_appointment(request):
    """来店予約削除"""
    now = datetime.now()
    customer = Customer.objects.get(id=request.customer.id)
    appointment_qs = Appointment.objects.filter(
        customer=customer,
        visit_at__gte=now,
    )
    if appointment_qs.exists():
        appointment_qs.delete()
        messages.success(request, "ご来店予約を削除しました。")
    else:
        messages.error(request, "ご来店は予約されていません。")
    return redirect('appointment:make_appointment')
